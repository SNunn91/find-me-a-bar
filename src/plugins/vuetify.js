import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme:{
    themes: {
      light: {
        primary: '#172b3a',
        secondary: '#1ab188',
        tertiary: '#e5eef5'
        
      },
    },
  },
  icons: {
    iconfont: 'mdi',
  },
  
});
