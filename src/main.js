import "@/assets/scss/site.scss";

import Vue from 'vue'
import App from './App.vue'
//FireStore
import VueFirestore from 'vue-firestore';
//Google Analytics
import VueAnalytics from 'vue-ua'

import router from './router'
import vuetify from './plugins/vuetify';

//initialise FireStore
Vue.use(VueFirestore);

//initialise Google Analytics
Vue.use(VueAnalytics, {
  // [Required] The name of your app as specified in Google Analytics.
  appName: 'Sports Bars',
  // [Required] The version of your app.
  appVersion: '1',
  // [Required] Your Google Analytics tracking ID.
  trackingId: 'UA-72114448-3',
  // If you're using vue-router, pass the router instance here.
  vueRouter: router
});

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
