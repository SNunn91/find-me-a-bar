import { firebase } from "@firebase/app";
import "@firebase/firestore";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyDEc0fBFraZCg1BcMCwJNcpinBo9ce3-54",
  authDomain: "nfl-bars.firebaseapp.com",
  databaseURL: "https://nfl-bars.firebaseio.com",
  projectId: "nfl-bars",
  storageBucket: "nfl-bars.appspot.com",
  messagingSenderId: "923441169957"
});

export const db = firebaseApp.firestore();